import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() { }

  rutas = [
    {
      name: 'Home',
      path: '/Home'
    },
    {
      name: 'About',
      path: '/about'
    },
    {
      name: 'Posts',
      path: '/posts'
    },
    {
      name: 'Contact',
      path: '/contact'
    }
  ];

  ngOnInit(): void {
  }

}
